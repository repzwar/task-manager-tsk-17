package ru.pisarev.tm;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import ru.pisarev.tm.bootstrap.Bootstrap;

public class ApplicationTest{

    @Rule
    public ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

    @Test
    public void displayInfo(){
        expectedSystemExit.expectSystemExitWithStatus(0);
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start("-i");
    }

    @Test
    public void displayVersion(){
        expectedSystemExit.expectSystemExitWithStatus(0);
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start("-v");
    }

}
