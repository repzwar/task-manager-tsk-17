package ru.pisarev.tm.command.project;

import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.enumerated.Sort;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListShowCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectService().findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(sortType.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }
}
