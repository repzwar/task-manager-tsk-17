package ru.pisarev.tm.command.system;

import ru.pisarev.tm.command.AbstractCommand;

public class DisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        int index = 1;
        for (String command : serviceLocator.getCommandService().getListCommandName()) {
            System.out.println(index + ". " + command);
            index++;
        }
    }

}
