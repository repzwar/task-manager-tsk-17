package ru.pisarev.tm.command.task;

import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllTaskByProjectIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-find-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task in project by project id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(id);
        System.out.println("Task list for project");
        for (Task task : tasks) {
            System.out.println(task.toString());
        }
    }
}
