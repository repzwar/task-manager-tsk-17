package ru.pisarev.tm.command.task;

import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.enumerated.Sort;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskShowListCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(sortType.getComparator());
        }
        int index = 1;
        for (Task project : tasks) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }
}
