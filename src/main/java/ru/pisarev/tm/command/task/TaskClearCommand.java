package ru.pisarev.tm.command.task;

import ru.pisarev.tm.command.TaskAbstractCommand;

public class TaskClearCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().clear();
    }
}
