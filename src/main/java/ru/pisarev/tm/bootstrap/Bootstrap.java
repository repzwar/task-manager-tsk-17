package ru.pisarev.tm.bootstrap;

import ru.pisarev.tm.api.repository.ICommandRepository;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.service.*;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.command.project.*;
import ru.pisarev.tm.command.system.*;
import ru.pisarev.tm.command.task.*;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.system.UnknownCommandException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.repository.CommandRepository;
import ru.pisarev.tm.repository.ProjectRepository;
import ru.pisarev.tm.repository.TaskRepository;
import ru.pisarev.tm.service.*;
import ru.pisarev.tm.util.TerminalUtil;

import static ru.pisarev.tm.util.TerminalUtil.displayWait;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

public final class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    public void start(String... args) {
        displayWelcome();
        if (runArgs(args)) System.exit(0);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ArgumentsDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindTaskToProjectByIdCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFindAllTaskByProjectIdCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindTaskByIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    {
        projectService.add(new Project("Project C", "-")).setStatus(Status.COMPLETED);
        projectService.add(new Project("Project A", "-"));
        projectService.add(new Project("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(new Project("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task C", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task A", "-"));
        taskService.add(new Task("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(new Task("Task D", "-")).setStatus(Status.COMPLETED);
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void process() {
        logService.debug("Test environment.");
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
