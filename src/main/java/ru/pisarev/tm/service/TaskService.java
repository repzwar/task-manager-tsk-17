package ru.pisarev.tm.service;

import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.service.ITaskService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(id);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(id);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(index);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(name);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

}
