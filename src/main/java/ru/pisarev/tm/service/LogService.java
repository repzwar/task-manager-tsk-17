package ru.pisarev.tm.service;

import ru.pisarev.tm.api.service.ILogService;

import java.io.IOException;
import java.util.logging.*;

public class LogService implements ILogService {

    private static final String COMMANDS = "COMMANDS";
    private static final String COMMANDS_FILE = "./commands.txt";

    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_FILE = "./messages.txt";

    private static final String ERRORS = "ERRORS";
    private static final String ERRORS_FILE = "./errors.txt";

    private final LogManager manager = LogManager.getLogManager();
    private final Logger root = Logger.getLogger("");
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger messages = Logger.getLogger(MESSAGES);
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(errors, ERRORS_FILE, true);
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(LogService.class.getResourceAsStream("/logger.properties"));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(final Logger logger, final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
