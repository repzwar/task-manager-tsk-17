package ru.pisarev.tm.api.service;

public interface ServiceLocator {
    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();
}
