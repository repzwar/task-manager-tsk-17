package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(final String projectId);

    Task bindTaskById(final String taskId, final String projectId);

    Task unbindTaskById(final String taskId);

    Project removeProjectById(final String projectId);

    Project removeProjectByIndex(final Integer index);

    Project removeProjectByName(final String name);

}
